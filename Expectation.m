function E = Expectation(X,k,W,M,V)
% X, samples
% k, number of gausses 
% W weight of gauss component
% M mean of every gauss component
% V covariance of every gauss component
% E expection of every gauss component
[n,d] = size(X); 
a = (2*pi)^(0.5*d); 
S = zeros(1,k); 
iV = zeros(d,d,k);
% prepare to compute coefficient of pdf of obvservations
for j=1:k,   
    if V(:,:,j)==zeros(d,d), 
        V(:,:,j)=ones(d,d)*eps;
    end
    S(j) = sqrt(det(V(:,:,j)));
    iV(:,:,j) = inv(V(:,:,j));
end
% start to compute pdf of obvs
E = zeros(n,k);
for i=1:n,  % for all samples
     for j=1:k, % for all gauss component
         dXM = X(i,:)'-M(:,j); % obs - mean
         pl = exp(-0.5*dXM'*iV(:,:,j)*dXM)/(a*S(j)); % pdf of obv  
         E(i,j) = W(j)*pl;  % product of weight and pdf of obv
     end
     E(i,:) = E(i,:)/sum(E(i,:)); % do normalization for all gauss component of the ith sample
end
%%%%%%%%%%%%%%%%%%%%%%%%
%%% End of Expectation %
%%%%%%%%%%%%%%%%%%%%%%%%     
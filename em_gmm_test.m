clear all;
samplenum = 6000;
sampledim = 2;
mixnum = 8;
X=zeros(samplenum,sampledim);
X(1:samplenum/3,:) = normrnd(0,1,samplenum/3,sampledim);
X(samplenum/3+1:samplenum/3*2,:) = normrnd(0,2,samplenum/3,sampledim);
X(samplenum/3*2+1:samplenum,:) = normrnd(0,3,samplenum/3,sampledim); 
[W,M,V,L] = EM_GM(X,mixnum,[],[],1,[]);

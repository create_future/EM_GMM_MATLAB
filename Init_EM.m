function[W,M,V] = Init_EM(X,k) 
[n,d] = size(X); 
[Ci,C] = kmeans(X,k,'Start','cluster', ...  
    'Maxiter',500, ... 
    'EmptyAction','drop', ...   
    'Display','off'); % Ci(nx1) - cluster indeices; C(k,d) - cluster centroid (i.e. mean) 
while sum(isnan(C))>0,
   [Ci,C] = kmeans(X,k,'Start','cluster', ...  
       'Maxiter',500, ...  
       'EmptyAction','drop', ...
       'Display','off'); 
end
M = C';  
Vp = repmat(struct('count',0,'X',zeros(n,d)),1,k); 
for i=1:n,
    % Separating all original cluster points into k different clusters 
    Vp(Ci(i)).count = Vp(Ci(i)).count + 1; 
    Vp(Ci(i)).X(Vp(Ci(i)).count,:) = X(i,:); 
end
V = zeros(d,d,k); 
for i=1:k, 
    W(i) = Vp(i).count/n;% compute initial weight of the ith gauss component
    V(:,:,i) = cov(Vp(i).X(1:Vp(i).count,:)); %compute initial conv of the ith gauss component
end